import winrm

# Create a WinRM session to the remote host
session = winrm.Session('http://remote-computer-ip:5985/wsman', auth=('username', 'password'))

# PowerShell command to copy file using PowerShell Remoting (assuming remoting is enabled)
source_file = 'C:\\path\\to\\local\\file.txt'  # Local machine path
destination_file = 'C:\\path\\to\\remote\\file.txt'  # Remote machine path

# PowerShell script to create a remote session and copy the file
powershell_script = f"""
$User = '{session.username}'
$Pass = ConvertTo-SecureString '{session.password}' -AsPlainText -Force
$Cred = New-Object System.Management.Automation.PSCredential($User, $Pass)
$session = New-PSSession -ComputerName remote-computer-ip -Credential $Cred
Copy-Item -Path '{source_file}' -Destination '{destination_file}' -ToSession $session
Remove-PSSession $session
"""

# Execute the script via WinRM
result = session.run_ps(powershell_script)

# Check the results
print(result.status_code)
print(result.std_out.decode())
print(result.std_err.decode())
