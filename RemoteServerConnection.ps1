function Write-Log {
    param(
        [Parameter(Mandatory=$true)]
        [string]$Message,

        [Parameter(Mandatory=$false)]
        [ValidateSet("Info", "Warning", "Error")]
        [string]$LogLevel = "Info",

        [Parameter(Mandatory=$true)]
        [string]$LogFilePath
    )

    # Get current date and time in a suitable format for logging
    $timestamp = Get-Date -Format "yyyy-MM-dd HH:mm:ss"

    # Create log entry format
    $logEntry = "[ $timestamp ] [ $LogLevel ] - $Message"

    # Write log entry to the specified log file
    try {
        Add-content -Path $LogFilePath -Value $logEntry -ErrorAction Stop
    } catch {
        Write-Error "Failed to write log entry to $LogFilePath: $_"
    }
}

function Get-ServerDNSName {
    param(
        [Parameter(Mandatory=$true)]
        [string]$ComputerName,

        [Parameter(Mandatory=$true)]
        [string]$LogFilePath
    )

    try {
        $dnsName = [System.Net.Dns]::GetHostEntry($ComputerName).HostName
        Write-Output "DNS Name of $ComputerName: $dnsName"
    } catch {
        $errorMessage = "An error occurred while retrieving DNS name for $ComputerName: $_"
        Write-Log -Message $errorMessage -LogLevel "Error" -LogFilePath $LogFilePath
        Write-Error $errorMessage
    }
}

function New-RemotePSSession {
    param(
        [Parameter(Mandatory=$true)]
        [string]$ComputerName
    )

    try {
        $session = New-PSSession -ComputerName $ComputerName -Credential $credential

        Write-Output "New PowerShell remoting session to $ComputerName created successfully."

        return $session
    } catch {
        Write-Error "Failed to create a new PowerShell remoting session to $ComputerName: $_"
        return $null
    }
}


function Test-RemoteFolderAvailability {
    param(
        [Parameter(Mandatory=$true)]
        [string]$Folder,

        [Parameter(Mandatory=$true)]
        [System.Management.Automation.PSSession]$Session
    )

    try {
        # Define parameters to be passed to the script block
        $parameters = @{
            Folder = $Folder
            ScriptBlock = {
                param($Folder)
                Test-Path -Path $Folder -PathType Container
            }
        }

        # Invoke-Command with parameters
        $folderExists = Invoke-Command -Session $Session @parameters

        return $folderExists
    } catch {
        Write-Error "An error occurred while testing folder availability: $_"
        return $false
    }
}

