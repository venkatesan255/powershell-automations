# Load the XML assembly
[xml]$xmlDocument = New-Object System.Xml.XmlDocument

# Create the root element
$root = $xmlDocument.CreateElement("Users")
$xmlDocument.AppendChild($root)

# Function to create a user element with text
function Add-User($id, $name, $email, $description) {
    $user = $xmlDocument.CreateElement("User")
    $user.SetAttribute("Id", $id)
    $user.SetAttribute("Name", $name)
    $user.SetAttribute("Email", $email)
    
    # Add text content to the User element
    $userText = $xmlDocument.CreateTextNode($description)
    $user.AppendChild($userText) | Out-Null
    
    $root.AppendChild($user) | Out-Null
}

# Add user elements with text
Add-User -id "1" -name "John Doe" -email "john.doe@example.com" -description "John is a software engineer."
Add-User -id "2" -name "Jane Smith" -email "jane.smith@example.com" -description "Jane is a project manager."
Add-User -id "3" -name "Sam Brown" -email "sam.brown@example.com" -description "Sam is a data analyst."


# Save the XML document to a file
$xmlDocument.Save("C:\temp\output.xml")

Write-Output "XML file created successfully."


# Load the XML assembly
[xml]$xmlDocument = New-Object System.Xml.XmlDocument

# Create the root element with the namespace
$namespace = "http://schemas.microsoft.com/developer/msbuild/2003"
$root = $xmlDocument.CreateElement("Project", $namespace)
$xmlDocument.AppendChild($root)

# Create the PropertyGroup element
$propertyGroup = $xmlDocument.CreateElement("PropertyGroup", $namespace)
$root.AppendChild($propertyGroup)

# Function to add a property element with text
function Add-Property($elementName, $elementValue) {
    $propertyElement = $xmlDocument.CreateElement($elementName, $namespace)
    $propertyText = $xmlDocument.CreateTextNode($elementValue)
    $propertyElement.AppendChild($propertyText) | Out-Null
    $propertyGroup.AppendChild($propertyElement) | Out-Null
}

# Add property elements
Add-Property -elementName "LRCRYPTO_STRING" -elementValue "test"
Add-Property -elementName "LR_HOST" -elementValue "he"
Add-Property -elementName "IMPROVEMENT_PROG" -elementValue "sfg"

# Save the XML document to a file
$xmlDocument.Save("C:\path\to\your\output.xml")

Write-Output "XML file created successfully."
